﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class starscript : MonoBehaviour
{
    public GameObject[] Stars;
    public AudioSource StarSound;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("ShowStarwithdelay");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ShowStars()
    {

    }
    IEnumerator ShowStarwithdelay()
    {
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < 3; i++)
        {
            StarSound.Play();
            Stars[i].SetActive(true);
            yield return new WaitForSeconds(.2f); 
        }

    }
}
