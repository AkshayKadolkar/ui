﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class UIcontroller : MonoBehaviour
{
    public string playername;
    public InputField Name;
    public Text outputtext;
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PlayPress()
    {
        print("Ready to play");
        outputtext.text = "Ready to play";
        playername = Name.text;
        SceneManager.LoadScene("playscene");
    }
    public void OptionPress()
    {
        print("Select your choice");
        outputtext.text = "Select your choice";
    }
    public void SoundPress()
    {
        print("Switched to on/of");
        outputtext.text = "Switched to on/of";
    }
    public void ExitPress()
    {
        print("Are you Sure");
        outputtext.text = "Are you Sure";
        Application.Quit();
    }
}
