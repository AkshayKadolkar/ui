﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class volume : MonoBehaviour
{
    public AudioSource Bgmusic;
    public Slider Volumeslider;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void changeVolume()
    {
        //print("changing volume");
        Bgmusic.volume = Volumeslider.value;
    }
}
