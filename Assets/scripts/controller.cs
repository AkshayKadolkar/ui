﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class controller : MonoBehaviour
{
    public AudioSource bgmusic;
    public Slider VolumeSlider;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Loadpreviousscreen();
    }
    public void Quitapplication()
    {
        // print("are you sure");
        Application.Quit();

    }
    public void Musiconof()
    {
        print("value=" + VolumeSlider.value);
        bgmusic.volume = VolumeSlider.value;
    }
    public void Gameview()
    {
        //print("xyz");

        SceneManager.LoadScene("gamescene");
    }
    public void Loadpreviousscreen()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();

                return;
            }

        }
    }
}  
